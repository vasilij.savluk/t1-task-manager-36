package ru.t1.chubarov.tm.model;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.model.IWBS;
import ru.t1.chubarov.tm.enumerated.Status;

import java.util.Date;

public final class Task extends AbstractUserOwnerModel implements IWBS {

    @NotNull
    private String name = "";
    @NotNull
    private String description = "";
    @NotNull
    private Status status = Status.NOT_STARTED;
    @Nullable
    private String projectId;
    @NotNull
    private Date created = new Date();

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description;
    }

    @NotNull
    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull Status status) {
        this.status = status;
    }

    @Nullable
    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(@Nullable String projectId) {
        this.projectId = projectId;
    }

    @NotNull
    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(@NotNull Date created) {
        this.created = created;
    }

}
