package ru.t1.chubarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.*;
import ru.t1.chubarov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISystemEndpoint extends IEndpoint {

    @NotNull
    String NAME = "SystemEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @NotNull
    @WebMethod
    ApplicationAboutResponse getAbout(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ApplicationAboutRequest request);

    @NotNull
    @WebMethod
    ApplicationVersionResponse getVersion(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ApplicationVersionRequest request);

    @NotNull
    @WebMethod
    ApplicationHelpResponse getHelp(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ApplicationHelpRequest request);

    @WebMethod(exclude = true)
    static void main(String[] args) {
        ISystemEndpoint.newInstance().getAbout(new ApplicationAboutRequest());
    }

}
