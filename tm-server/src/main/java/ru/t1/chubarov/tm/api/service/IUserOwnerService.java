package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.IUserOwnerRepository;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerService<M extends AbstractUserOwnerModel> extends IUserOwnerRepository<M>, IService<M>{

    void removeAll(@Nullable String userId) throws AbstractException;

    boolean existsById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    M add(@Nullable String userId, @Nullable  M model) throws AbstractException;

    @Nullable
    List<M> findAll(@NotNull String userId);

    @Nullable
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator) throws AbstractException;

    @NotNull
    M removeOne(@NotNull String userId, @Nullable M model) throws AbstractException;

    @NotNull
    M findOneById(@NotNull String userId, @Nullable String id) throws AbstractException;

    @NotNull
    M findOneByIndex(@NotNull String userId, @Nullable Integer index) throws AbstractException;

    @NotNull
    M removeOneById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @Nullable
    M removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws AbstractException;

    int getSize(@Nullable String userId) throws AbstractException;

}
