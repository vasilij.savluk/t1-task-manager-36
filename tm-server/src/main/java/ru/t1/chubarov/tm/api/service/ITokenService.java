package ru.t1.chubarov.tm.api.service;

public interface ITokenService {

    void setToken(String token);

    String getToken();

}
