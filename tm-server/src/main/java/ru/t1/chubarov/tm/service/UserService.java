package ru.t1.chubarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.IProjectRepository;
import ru.t1.chubarov.tm.api.repository.ITaskRepository;
import ru.t1.chubarov.tm.api.repository.IUserRepository;
import ru.t1.chubarov.tm.api.service.IPropertyService;
import ru.t1.chubarov.tm.api.service.IUserService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.exception.user.UserNotFoundException;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.util.HashUtil;

import java.util.Collection;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectRepository projectRepository;
    @NotNull
    private final ITaskRepository taskRepository;
    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IPropertyService propertyService,
                       @Nullable final IUserRepository repository,
                       @Nullable final ITaskRepository taskRepository,
                       @Nullable final IProjectRepository projectRepository) {
        super(repository);
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws AbstractException {
        if (isEmailExist(email)) throw new ExistsEmailException(login, email);
        @Nullable final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws AbstractException {
        if (role == null) throw new RoleEmptyException();
        @NotNull final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = repository.findByLogin(login);
        if (user == null) return null;
        return user;
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = repository.findByEmail(email);
        if (user == null) return null;
        return user;
    }

    @Nullable
    @Override
    public User removeOne(@Nullable final User model) throws AbstractException {
        if (model == null) return null;
        @Nullable final User user = super.remove(model);
        @Nullable final String userId = user.getId();
        taskRepository.removeAll(userId);
        projectRepository.removeAll(userId);
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        return removeOne(user);
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException("login", email);
        @Nullable final User user = findByEmail(email);
        if (user == null) return null;
        return removeOne(user);
    }

    @NotNull
    @Override
    public User setPassword(@Nullable final String id, @Nullable final String password) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = findOneById(id);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return user;
    }

    @NotNull
    @Override
    public User updateUser(@Nullable final String id,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final User user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
    }

}
