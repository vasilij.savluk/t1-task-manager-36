package ru.t1.chubarov.tm.repository;

import ru.t1.chubarov.tm.api.repository.ISessionRepository;
import ru.t1.chubarov.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnerRepository<Session> implements ISessionRepository {

}
