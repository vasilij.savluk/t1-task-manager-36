package ru.t1.chubarov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.repository.IProjectRepository;
import ru.t1.chubarov.tm.enumerated.ProjectSort;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String userIdFirst = UUID.randomUUID().toString();

    @NotNull
    private final String userIdSecond = UUID.randomUUID().toString();

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectRepository projectRepository;

    @SneakyThrows
    @Before
    public void initRepository() {
        projectList = new ArrayList<>();
        projectRepository = new ProjectRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("project_" + i);
            project.setDescription("project description_" + i);
            if (i <= 5) project.setUserId(userIdFirst);
            else project.setUserId(userIdSecond);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testAdd() {
        int numberOfProject = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Project Description";
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        @NotNull final Project actualproject = projectRepository.findOneByIndex(numberOfProject - 1);
        Assert.assertNotNull(actualproject);
        Assert.assertEquals(userId, actualproject.getUserId());
        Assert.assertEquals(name, actualproject.getName());
        Assert.assertEquals(description, actualproject.getDescription());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testAddUserNegative() {
        int numberOfProject = NUMBER_OF_ENTRIES;
        @NotNull final String userId = null;
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Project Description";
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testRemoveAllUser() {
        projectRepository.removeAll(userIdFirst);
        Assert.assertEquals(5, projectRepository.getSize());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testFindSort() {
        @NotNull final ProjectSort sort = ProjectSort.toSort("BY_NAME");
        @NotNull final List<Project> actualProjectList = projectRepository.findAll(userIdFirst, sort.getComparator());
        @NotNull final List<Project> userProjectList = projectRepository.findAll(userIdFirst);
        Assert.assertEquals(5, userProjectList.size());
        Assert.assertEquals(5, actualProjectList.size());
        userProjectList.sort(sort.getComparator());
        Assert.assertEquals(userProjectList, actualProjectList);
    }

    @Test
    @Category(UnitCategory.class)
    public void testFindAllUser() {
        @NotNull final List<Project> actualProjectList = projectRepository.findAll(userIdFirst);
        Assert.assertEquals(5, actualProjectList.size());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testFindOneById() {
        int numberOfProject = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        @NotNull final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        @NotNull final Project actualProjectIndex = projectRepository.findOneByIndex(userId, numberOfProject - 1);
        Assert.assertNotNull(actualProjectIndex);
        @NotNull final String projecyId = actualProjectIndex.getId();
        Assert.assertNotNull(projecyId);
        Assert.assertEquals(projecyId, projectRepository.findOneById(userId, projecyId).getId());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testRemoveOne() {
        projectRepository.removeOne(userIdFirst, projectList.get(1));
        Assert.assertEquals(9, projectRepository.getSize());
        Assert.assertEquals(4, projectRepository.getSize(userIdFirst));
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testRemoveOneById() {
        int numberOfProject = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final Project project = new Project();
        project.setName("Test Project");

        projectRepository.add(userId, project);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        @NotNull final Project actualProjectIndex = projectRepository.findOneByIndex(userId, numberOfProject - 1);
        Assert.assertNotNull(actualProjectIndex);
        Assert.assertNull(projectRepository.removeOneById(userId, "fail_test_id"));
        projectRepository.removeOneById(userId, actualProjectIndex.getId());
        Assert.assertEquals(numberOfProject - 1, projectRepository.getSize());

        projectRepository.add(userId, project);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        projectRepository.removeOneById(userId, projectRepository.findOneByIndex(numberOfProject - 1).getId());
        Assert.assertEquals(numberOfProject - 1, projectRepository.getSize());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testRemoveOneByIndex() {
        int numberOfProject = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final Project project = new Project();
        project.setName("Test Project");

        projectRepository.add(userId, project);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        projectRepository.removeOneByIndex(userId, numberOfProject - 1);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());

        projectRepository.add(userId, project);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        Assert.assertNotNull(projectRepository.removeOneByIndex(userId, numberOfProject - 1));
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testExistById() {
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        @NotNull final Project project = new Project();
        project.setName(name);
        @NotNull final Project actualProject = projectRepository.add(userId, project);
        Assert.assertEquals(true, projectRepository.existsById(actualProject.getId()));
        Assert.assertEquals(true, projectRepository.existsById(userId, actualProject.getId()));
        Assert.assertEquals(false, projectRepository.existsById("1111"));
        Assert.assertEquals(false, projectRepository.existsById(userId, "1111"));
        Assert.assertEquals(false, projectRepository.existsById("userId-123-4", actualProject.getId()));
    }

}
