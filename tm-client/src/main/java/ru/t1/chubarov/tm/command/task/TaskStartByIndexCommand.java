package ru.t1.chubarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.TaskChangeStatusByIndexRequest;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY INDEX");
        System.out.println("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final Status status = Status.toStatus("IN_PROGRESS");
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(getToken());
        request.setIndex(index);
        request.setStatus(status);
        getTaskEndpoint().changeTaskStatusByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-start-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update task status to [In progress] by index.";
    }

}
